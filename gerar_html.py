#!/usr/bin/python3
#
# Script que gera a tabuada do 'n'
# o valor de 'n' é recebido através da variável n da QUERY_STRING
#
import cgi



print('Content-type: text/html; charset=utf-8')
print()

#
# cabecalho é uma string que contém a primeira parte do HTML de resposta
#
cabecalho = '''
<h1>
<center>Tabuada</center>
</h1>
'''

#
# Lemos o valor de 'n' que recebemos do browser
#
n_tabuada = int(cgi.FieldStorage().getvalue('n'))


#
# Começamos a montar a lista que conterá os valores da tabuada
# Geramos primeiro os ítens da lista (<li>) e guardamos 
# na variável html_val_tabuada
# Depois é que inserimos esses ítens entre as tags <ul></ul>
# na parte seguinte do código (html_tabuada)
#


# Começamos com uma string vazia
html_val_tabuada = ''
#
# Geramos uma linha da lista e concatemos essa linha com a linha
# que já tínhamos guardada na variável html_val_tabuada
#
# Ao final deste for vamos ter uma string (html_val_tabuada)
# que contém a concatenação de todas as linhas geras. Ou seja:
# html_val_tabuada vai conter um trecho de código HTML parecido com
# o seguinte (para n = 7):
#
# <li>7 x 1 = 7</li>
# <li>7 x 2 = 14</li>
# <li>7 x 3 = 21</li>
# <li>7 x 4 = 28</li>
# <li>7 x 5 = 35</li>
# <li>7 x 6 = 42</li>
# <li>7 x 7 = 49</li>
# <li>7 x 8 = 56</li>
# <li>7 x 9 = 63</li>
# <li>7 x 10 = 70</li>

for numero in range(1,11):
    html_val_tabuada = html_val_tabuada + f'<li>{n_tabuada} x {numero} = {n_tabuada * numero }</li>\n'

# Agora geramos o HTML do corpo da resposta - a lista com a tabuada
# esta parte contém o cabeçalho da lista e os ítens da lista que
# geramos acima

html_tabuada = f'''
<h3>Tabuada do {n_tabuada}</h3>
<ul>
{html_val_tabuada}
</ul>
'''

#
# Só agora é que mandamos como resposta o HTML que geramos.
# Mandamos em 2 partes (2 prints)
#
print(cabecalho)
print(html_tabuada)

